package dominio;

import dominio.repositorio.RepositorioProducto;
import dominio.repositorio.RepositorioGarantiaExtendida;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Vendedor {

    public static final String EL_PRODUCTO_TIENE_GARANTIA = "El producto ya cuenta con una garantia extendida";
    public static final String EL_PRODUCTO_NO_TIENE_GARANTIA="Este producto no cuenta con garant�a extendida";
    public static final  double  PRECIO_BASE= 500.000;
    public static final  int  DIAS_GARANTIA_UNO=  200;
    public static final  int  DIAS_GARANTIA_DOS=  100;

    private RepositorioProducto repositorioProducto;
    private RepositorioGarantiaExtendida repositorioGarantia;

    public   Vendedor(){
    }

    public Vendedor(RepositorioProducto repositorioProducto, RepositorioGarantiaExtendida repositorioGarantia) {
        this.repositorioProducto = repositorioProducto;
        this.repositorioGarantia = repositorioGarantia;

    }

    public void generarGarantia(String codigo, String  nombreCliente) throws ParseException {

        List<String>  precioTiempo =  new ArrayList<>();
        double precio =0;
        precioTiempo =calcularPrecioTiempo(repositorioProducto.obtenerPorCodigo(codigo).getPrecio());
        Date fechaInicio =  new Date();
        Date fechafin =  new SimpleDateFormat("dd-mm-yyyy").parse(precioTiempo.get(1));
        precio= Double.parseDouble(precioTiempo.get(0));


        if(tieneGarantia(codigo)== true )
              throw new UnsupportedOperationException(EL_PRODUCTO_TIENE_GARANTIA);
        if(codigoIsvalid(codigo) ==  true)
              throw new UnsupportedOperationException(EL_PRODUCTO_NO_TIENE_GARANTIA);
          GarantiaExtendida garantiaExtendida=  new GarantiaExtendida(
                  repositorioProducto.obtenerPorCodigo(codigo),
                  fechaInicio,
                  fechafin,precio, nombreCliente
          );
        repositorioGarantia.agregar(garantiaExtendida);

    }

    public boolean tieneGarantia(String codigo) {

        if(repositorioGarantia.obtenerProductoConGarantiaPorCodigo(codigo)!= null)
            return true;
        return false;
    }

    public   boolean  codigoIsvalid( String  codigo){

        String  regex  = "[aeiou]{2}/i";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher =  pattern.matcher(codigo);
        boolean buscar =  matcher.find();

        return  buscar;
    }

    public List<String> calcularPrecioTiempo(double  precio){

          List<String>  list  = new ArrayList<>();
          double costoGarantia= 0;
           String  fechaFinGarantia="";
          if( precio >= PRECIO_BASE ){
              costoGarantia=precio*0.20;
              fechaFinGarantia=diasGrantia(DIAS_GARANTIA_UNO);
          }else {
              costoGarantia= precio*0.10;
              fechaFinGarantia=diasGrantia(DIAS_GARANTIA_DOS);
          }
          list.add(String.valueOf(costoGarantia));
          list.add(fechaFinGarantia);
        return  list;

    }

   public String  diasGrantia( int dias){

        LocalDate localDateNow =  LocalDate.now();
        LocalDate fechafinTem =  localDateNow.plusDays(dias);
        int count = 0;

        while((localDateNow= localDateNow.plusDays(1)).isBefore(fechafinTem.plusDays(1))){

            if (localDateNow.getDayOfWeek().toString() ==  "MONDAY" ){
                count = count+1;
            }
       }
        return String.valueOf(localDateNow.plusDays(dias+count));
   }
}
