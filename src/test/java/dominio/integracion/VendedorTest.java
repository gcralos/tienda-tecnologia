package dominio.integracion;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import dominio.Vendedor;
import dominio.Producto;
import dominio.excepcion.GarantiaExtendidaException;
import dominio.repositorio.RepositorioProducto;
import dominio.repositorio.RepositorioGarantiaExtendida;
import persistencia.sistema.SistemaDePersistencia;
import testdatabuilder.ProductoTestDataBuilder;

import java.text.ParseException;

public class VendedorTest {

	private static final String CODIGO= "F01TSA0150";
	private static final String NOMBRE = "Computador Lenovo";
	private static final double PRECIO = 780000;
	private static final  String NOMBRE_CLIENTE="Carlos Arturo Gonzalez";

	private SistemaDePersistencia sistemaPersistencia;

	private RepositorioProducto repositorioProducto;
	private RepositorioGarantiaExtendida repositorioGarantia;

	@Before
	public void setUp() {
		
		sistemaPersistencia = new SistemaDePersistencia();
		repositorioProducto = sistemaPersistencia.obtenerRepositorioProductos();
		repositorioGarantia = sistemaPersistencia.obtenerRepositorioGarantia();
		sistemaPersistencia.iniciar();


	}
	

	@After
	public void tearDown() {
		sistemaPersistencia.terminar();
	}

	@Test
	public void generarGarantiaTest() throws ParseException {
		// arrange
		Producto producto = new ProductoTestDataBuilder()
				.conCodigo(CODIGO)
				.conNombre(NOMBRE)
				.conPrecio(PRECIO).build();
		repositorioProducto.agregar(producto);
		Vendedor vendedor = new Vendedor(repositorioProducto, repositorioGarantia);

		// act
		vendedor.generarGarantia(producto.getCodigo(),NOMBRE_CLIENTE);

		// assert
		Assert.assertTrue(vendedor.tieneGarantia(producto.getCodigo()));
		Assert.assertNotNull(repositorioGarantia.obtenerProductoConGarantiaPorCodigo(producto.getCodigo()));

	}

	@Test
	public void productoYaTieneGarantiaTest() throws ParseException {
		Producto producto = new ProductoTestDataBuilder()
				.conCodigo(CODIGO)
				.conNombre(NOMBRE)
				.conPrecio(PRECIO).build();
		// arrange
		repositorioProducto.agregar(producto);
		Vendedor vendedor = new Vendedor(repositorioProducto, repositorioGarantia);
		vendedor.generarGarantia(producto.getCodigo(),NOMBRE_CLIENTE);
		// act

		try {

			Assert.assertTrue(vendedor.tieneGarantia(producto.getCodigo()));
			
		} catch (GarantiaExtendidaException e) {
			// assert
			Assert.assertEquals(Vendedor.EL_PRODUCTO_TIENE_GARANTIA, e.getMessage());
		}
	}
}
